<?php
/**
 * @file
 * Contains \Drupal\css_background_animation\Form\ContentManagementForm.
 */
namespace Drupal\css_background_animation\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file\FileUsage\FileUsageInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class cssBackgroundAnimationAdmin extends ConfigFormBase {
	
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'css_background_animation_form';
  }

  /**
   * {@inheritdoc}
   */
		public function buildForm(array $form, FormStateInterface $form_state) {
				$form = parent::buildForm($form, $form_state);
				$config = $this->config('css_background_animation.settings');

				// Gather the number of names in the form already.
				$count = $form_state->get('count');
				//To ensure that there is at least one name field.
				if ($config->get('css_background_animation.count') === NULL) {
						$count = 1;
				} 
				if ($count === NULL) {
						$form_state->set('count', $config->get('css_background_animation.count') - 1);
						$count = $config->get('css_background_animation.count') - 1;
				}
				$form['#tree'] = TRUE;
				$form['home'] = [
						'#type' => 'fieldset',
						'#prefix' => '<div id="names-fieldset-wrapper">',
						'#suffix' => '</div>',
				];

				for ($i = 0; $i < $count; $i++) {
						$form['home'][$i]['background_item'] = [
								'#type' => 'fieldset',
								'#prefix' => '<div id="names-fieldset-wrapper-item">',
								'#suffix' => '</div>',
						];
						$form['home'][$i]['background_item']['css_selector'] = [
								'#type' => 'textfield',
								'#title' => $this->t('Selector'),
								'#description' => 'Only one selector allowed. Eg: #main-wrapper',
								'#default_value' => $config->get('css_background_animation.css_selector' . $i),
						];
						$form['home'][$i]['background_item']['shape'] = [
								'#type' => 'select',
								'#title' => $this->t('Animation Shape'),
								'#options' => [
										'circle' => $this->t('Circle'),
										'square' => $this->t('Square'),
										'bubble' => $this->t('Bubble'),
								],
								'#empty_option' => $this->t('-select-'),
								'#default_value' => $config->get('css_background_animation.shape' . $i),
						];
						$form['home'][$i]['background_item']['color'] = [
								'#type' => 'color',
								'#title' => $this->t('Color'),
								'#default_value' => $config->get('css_background_animation.color' . $i),
								'#description' => 'Color of the shape. Border color in case of circle',
						];
						$form['home'][$i]['background_item']['small_count'] = [
								'#type' => 'number',
								'#title' => $this->t('Number of small items'),
								'#description' => $this->t('Number of small size items to display'),
								'#default_value' => $config->get('css_background_animation.small_count' . $i),
						];
						$form['home'][$i]['background_item']['medium_count'] = [
								'#type' => 'number',
								'#title' => $this->t('Number of medium items'),
								'#description' => $this->t('Number of medium size items to display'),
								'#default_value' => $config->get('css_background_animation.medium_count' . $i),
						];
						$form['home'][$i]['background_item']['large_count'] = [
								'#type' => 'number',
								'#title' => $this->t('Number of large items'),
								'#description' => $this->t('Number of large size items to display'),
								'#default_value' => $config->get('css_background_animation.large_count' . $i),
						];
						$form['home'][$i]['background_item']['transition'] = [
								'#type' => 'select',
								'#title' => $this->t('Transition Effect'),
								'#options' => [
										'updown' => $this->t('Move Up Down'),
										'leftright' => $this->t('Move Left Right'),
										'grow' => $this->t('Grow'),
										'glow' => $this->t('Glow'),
								],
								'#empty_option' => $this->t('-select-'),
								'#default_value' => $config->get('css_background_animation.transition' . $i),
						];
				}

				$form['home']['actions']['add_name'] = [
						'#type' => 'submit',
						'#value' => $this->t('Add one more'),
						'#submit' => ['::addOne'],
						'#ajax' => [
								'callback' => '::addmoreCallback',
								'wrapper' => 'names-fieldset-wrapper',
						],
				];

				if ($count > 1) {
						$form['home']['actions']['remove_name'] = [
								'#type' => 'submit',
								'#value' => $this->t('Remove one'),
								'#submit' => ['::removeCallback'],
								'#ajax' => [
										'callback' => '::addmoreCallback',
										'wrapper' => 'names-fieldset-wrapper',
								],
						];
				}
				$form['#attached']['library'][] = 'css_background_animation/css_bg_administer_form';
				return $form;
		}

  /**
   * {@inheritdoc}
   */
  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    return $form['home'];
  }

  /**
   * {@inheritdoc}
   */
  public function addOne(array &$form, FormStateInterface $form_state) {
    $count = $form_state->get('count');
    $addone = $count + 1;
    $form_state->set('css_background_animation.count', $add_button);
    $form_state->set('count', $addone);
    // Since our buildForm() method relies on the value of 'num_names' to
    // generate 'name' form elements, we have to tell the form to rebuild. If we
    // don't do this, the form builder will not call buildForm().
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
		public function removeCallback(array &$form, FormStateInterface $form_state) {
				$count = $form_state->get('count');
				if ($count > 1) {
						$count = $count - 1;
						$form_state->set('count', $count);
				}
				$form_state->setRebuild();
		}

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('css_background_animation.settings');
    $config->delete('css_background_animation');
    $config->set('css_background_animation.count', count($form_state->getValues()['home']))->save();
    for ($multiset = 0; $multiset < count($form_state->getValues()['home']) - 1; $multiset++) {
      $data = NULL;
      $submitted_value = $form_state->getValues()['home'][$multiset]['background_item'];
      foreach ($submitted_value as $key => $value) {
        $data.= $key . '||' . $value . '&&';
      }
      $config->set('css_background_animation.selector.data' . $multiset, $data)->save();
      $config->set('css_background_animation.css_selector' . $multiset, $submitted_value['css_selector'])->save();
      $config->set('css_background_animation.shape' . $multiset, $submitted_value['shape'])->save();
      $config->set('css_background_animation.color' . $multiset, $submitted_value['color'])->save();
      $config->set('css_background_animation.small_count' . $multiset, $submitted_value['small_count'])->save();
      $config->set('css_background_animation.medium_count'. $multiset, $submitted_value['medium_count'])->save();
      $config->set('css_background_animation.large_count' . $multiset, $submitted_value['large_count'])->save();
      $config->set('css_background_animation.transition' . $multiset, $submitted_value['transition'])->save();
    }
    $config->save();

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'css_background_animation.settings',
    ];
  }

}
