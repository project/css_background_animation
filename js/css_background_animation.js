jQuery(document).ready(function ($) {
    var css_selector, shape, transition, small_count;
    var completedata = drupalSettings.css_background_animation;
    $.each(completedata.selector, function ( selectorkey, selectorvalue ) {
        var selecteach = selectorvalue.split('&&');
        var selector_exists = 'No';
        $.each(selecteach, function ( selectoreachkey, selectoreachvalue ) {
            var selecteachitem = selectoreachvalue.split('||');
            if (selecteachitem[0] == 'css_selector') {
                if(selecteachitem[1] != undefined && $(selecteachitem[1]).length == 1) {
                    selector_exists = 'Yes';
                }
            }
            if(selector_exists == 'Yes') {
                switch(selecteachitem[0]) {
                    case 'css_selector':
                        css_selector = selecteachitem[1];
                        break;

                    case 'shape':
                        shape = selecteachitem[1];
                        break;

                    case 'color':
                        color = selecteachitem[1];
                        break;

                    case 'small_count':
                        small_count = selecteachitem[1];
                        break;

                    case 'medium_count':
                        medium_count = selecteachitem[1];
                        break;

                    case 'large_count':
                        large_count = selecteachitem[1];
                        break;
                }
                if (selecteachitem[0] == 'transition') {
                    transition = selecteachitem[1];
                    $(css_selector).css({"position": "relative", "z-index":"0"});
                    if(small_count != undefined && small_count != 0) {
                        elementBuilder(css_selector,shape,transition, size = 's', small_count, shapeSize = 50);
                    }
                    if(medium_count != undefined && medium_count != 0) {
                        elementBuilder(css_selector,shape,transition, size = 'm', medium_count, shapeSize = 65);
                    }
                    if(large_count != undefined && large_count != 0) {
                        elementBuilder(css_selector,shape,transition, size = 'l', large_count, shapeSize = 80);
                    }
                }
            }
        });
    });

    function elementBuilder(css_selector,shape,transition, size, count, elementSize) {
        if(shape != undefined) {
            // Remove all existing shapes of current size
            $('.' + shape + '-' + size).remove();
            if(count != undefined) {
                var divWidth = $(css_selector).width();
                var divHeight = $(css_selector).height();
                var horizCord = [];
                var vertCord = [];
                var heightInterval = ($(css_selector).height() - elementSize) / 10;
                var widthInterval = ($(css_selector).width() - elementSize) / count;
                for (var i = elementSize; i < (divWidth - elementSize); i += (elementSize + 5)) {
                    horizCord.push(i);
                }
                for (var i = 0; i < (divHeight - elementSize); i += 5) {
                    vertCord.push(i);
                }

                for (var i = 1; i <= count; i++) {
                    var horizCordValue = horizCord[Math.floor(Math.random() * horizCord.length)];
                    horizCord = jQuery.grep(horizCord, function (value) {
                    return value != horizCordValue;
                    });

                    var vertCordValue = vertCord[Math.floor(Math.random() * vertCord.length)];
                    vertCord = jQuery.grep(vertCord, function (value) {
                        return value != vertCordValue;
                    });

                    // Append shapes
                    $(css_selector).prepend('<div class="animation-' + shape + ' ' + shape + '-' + size + ' ' + i + '" style="width:' + elementSize + 'px; height:' + elementSize + 'px; left:' + horizCordValue + 'px; bottom: ' + vertCordValue + 'px"></div>');
                    // Applying color
                    if(color != undefined) {
                        $(':root').css("--color", color);
                    }

                    // Adding transition class with an extension
                    if(transition != undefined) {
                        var dividend = 4;
                        var reminder = i % dividend;
                        $('.' + shape + '-' + size + '.' + i).addClass('animation-' + transition + '-' + reminder);
                    }
                }
            }
        }
    } // elementBuilder ends here
});
